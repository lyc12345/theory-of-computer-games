#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <unistd.h>
#include <assert.h>
#include <cmath>

#define BOARDSIZE        9
#define BOUNDARYSIZE    11
#define COMMANDLENGTH 1000
#define DEFAULTTIME     10
#define DEFAULTKOMI      7
#define MAXGAMELENGTH 1000
#define MAXSTRING       50
#define MAXDIRECTION     4
#define NUMINTERSECTION 81
#define HISTORYLENGTH   200
#define EMPTY            0
#define BLACK            1
#define WHITE            2
#define BOUNDARY         3
#define SELF             1
#define OPPONENT         2
#define NUMGTPCOMMANDS      15
#define LOCALVERSION      1
#define GTPVERSION        2
using namespace std;
struct node{
    bool isleaf;
    int MoveList[HISTORYLENGTH];
    bool enable[HISTORYLENGTH];
    node *child[HISTORYLENGTH];
    node *father;
    int win_play;
    int total_play;
    int num_legal_moves;
    double piece_sum;
    double piece_square;
    double lower,upper;
    node(){
        for(int i=0;i<HISTORYLENGTH;i++) {
            child[i]=NULL;
            enable[i]=true;
        }
        lower=-1e9;
        upper=1e9;
        win_play = 0;
        piece_sum = 0;
        piece_square = 0;
        total_play = 0;
        num_legal_moves = 0;
        isleaf = true;
        father = NULL;
    }
}*root;

int _board_size = BOARDSIZE;
int _board_boundary = BOUNDARYSIZE;
double _komi =  DEFAULTKOMI;
const int DX[MAXDIRECTION] = {-1, 0, 1, 0};
const int DY[MAXDIRECTION] = { 0, 1, 0,-1};
const char LabelX[]="0ABCDEFGHJ";
const double CONSTANT=2;
const int rd = 1;


int RecordHash[100][2000];
int RecordSize[100];
int numberHash[2000];
int total_hash_num;
bool StringFlag[90];
pair<int,int>xyArray[200];
int flag[BOUNDARYSIZE][BOUNDARYSIZE];
int empty[BOUNDARYSIZE][BOUNDARYSIZE];
int F,E;

int cnt=0;
double alltime,calctime;
int HashBoard[4][BOUNDARYSIZE][BOUNDARYSIZE];

void deleteNode(node *now){
    for(int i=0;i<now->num_legal_moves;i++)
        if(now->child[i]!=NULL)
          deleteNode(now->child[i]);
    delete now;
}

void reset(int Board[BOUNDARYSIZE][BOUNDARYSIZE]) {
    for (int i = 1 ; i <= BOARDSIZE; ++i) {
        for (int j = 1 ; j <= BOARDSIZE; ++j) {
            Board[i][j] = EMPTY;
        }
    }
    for (int i = 0 ; i < BOUNDARYSIZE; ++i) {
        Board[0][i] = Board[BOUNDARYSIZE-1][i] = Board[i][0] = Board[i][BOUNDARYSIZE-1] = BOUNDARY;
    }
}

int find_liberty(int X, int Y, int label, int Board[BOUNDARYSIZE][BOUNDARYSIZE], int ConnectBoard[BOUNDARYSIZE][BOUNDARYSIZE]) {
    ConnectBoard[X][Y] |= (1<<label);
    int total_liberty = 0;
    for (int d = 0 ; d < MAXDIRECTION; ++d) {
        if ((ConnectBoard[X+DX[d]][Y+DY[d]] & (1<<label) )!= 0) continue;
        ConnectBoard[X+DX[d]][Y+DY[d]] |=(1<<label);
        if (Board[X+DX[d]][Y+DY[d]] == EMPTY){
            total_liberty++;
        }
        else if (Board[X+DX[d]][Y+DY[d]] == Board[X][Y]) {
            total_liberty += find_liberty(X+DX[d], Y+DY[d], label, Board, ConnectBoard);
        }
    }
    return total_liberty;
}

void count_liberty(int X, int Y, int Board[BOUNDARYSIZE][BOUNDARYSIZE], int Liberties[MAXDIRECTION]) {
    int ConnectBoard[BOUNDARYSIZE][BOUNDARYSIZE];
    for (int i = 0 ; i < BOUNDARYSIZE; ++i) {
        for (int j = 0 ; j < BOUNDARYSIZE; ++j) {
            ConnectBoard[i][j] = 0;
        }
    }
    for (int d = 0 ; d < MAXDIRECTION; ++d) {
	      Liberties[d] = 0;
	      if (Board[X+DX[d]][Y+DY[d]] == BLACK ||  Board[X+DX[d]][Y+DY[d]] == WHITE) {
	          Liberties[d] = find_liberty(X+DX[d], Y+DY[d], d, Board, ConnectBoard);
	      }
    }
}

inline void count_neighboorhood_state(int Board[BOUNDARYSIZE][BOUNDARYSIZE], int X, int Y, int turn, int* empt, int* self, int* oppo ,int* boun, int NeighboorhoodState[MAXDIRECTION]) {
    for (int d = 0 ; d < MAXDIRECTION; ++d) {
	      if(Board[X+DX[d]][Y+DY[d]] == turn) {
			      (*self)++;
			      NeighboorhoodState[d] = SELF;
       }
        else if(Board[X+DX[d]][Y+DY[d]] == 3-turn) {
			      (*oppo)++;
			      NeighboorhoodState[d] = OPPONENT;
        }
        else if(Board[X+DX[d]][Y+DY[d]] == EMPTY) {
            (*empt)++; 
			      NeighboorhoodState[d] = EMPTY;
        }
        else{ 
            (*boun)++;
			      NeighboorhoodState[d] = BOUNDARY;
	      }
    }
}

void find_all_liberty(int Board[BOUNDARYSIZE][BOUNDARYSIZE],int StringLiberties[90],int AllStrings[BOUNDARYSIZE][BOUNDARYSIZE],int StringNum[90],int StringHash[90],pair<int,int>input[200],int in,int StringStack[90],int &stk){
    int Qx[100],Qy[100];
    int Rx[100],Ry[100];
    int qs,qe;
    F++;
    for(int _i=0;_i<in;_i++){
        int i=input[_i].first;
        int j=input[_i].second;
        //if(Board[i][j]!=BLACK && Board[i][j] !=WHITE) continue;
        if(flag[i][j]!=F){
            E++;
            assert(stk>0);
            int sid=StringStack[stk-1];
            stk--;
            flag[i][j] = F;
            qs=qe=0;
            Qx[qe] = i;
            Qy[qe] = j;
            Rx[qs] = i;
            Ry[qs] = j;
            qe++;
            qs++;
            StringHash[sid] = HashBoard[Board[i][j]][i][j];
            AllStrings[i][j] = sid;
            int turn = Board[i][j];
            StringNum[sid] = 1;
            int liberty=0;
            while(qe!=0){
                int x=Qx[qe-1];
                int y=Qy[qe-1];
                qe--;
                for(int d=0;d<MAXDIRECTION;d++){
                    int dx = DX[d];
                    int dy = DY[d];
                    int tmp = Board[x+dx][y+dy];
                    if(tmp == EMPTY && empty[x+dx][y+dy] !=E){
                        liberty++;
                        empty[x+dx][y+dy] = E;
                    }
                    else if(tmp == turn && flag[x+dx][y+dy]!=F){
                        Qx[qe]=x+dx;
                        Qy[qe]=y+dy;
                        Rx[qs]=x+dx;
                        Ry[qs]=y+dy;
                        qe++;
                        qs++;
                        flag[x+dx][y+dy] = F;
                        StringHash[sid] += HashBoard[Board[x+dx][y+dy]][x+dx][y+dy];
                        StringNum[sid]++;
                        AllStrings[x+dx][y+dy] = sid;
                    }
                }
            }
            //StringNum[sid] = qs;
            StringLiberties[sid] = liberty;
        }
    }
}

int remove_piece(int Board[BOUNDARYSIZE][BOUNDARYSIZE], int X, int Y, int turn) {
    int remove_stones = (Board[X][Y]==EMPTY)?0:1;
    Board[X][Y] = EMPTY;
    for (int d = 0; d < MAXDIRECTION; ++d) {
	      if (Board[X+DX[d]][Y+DY[d]] == turn) {
	          remove_stones += remove_piece(Board, X+DX[d], Y+DY[d], turn);
	      }
    }
    return remove_stones;
}


void update_board(int Board[BOUNDARYSIZE][BOUNDARYSIZE], int X, int Y, int turn) {
    int num_neighborhood_self = 0;
    int num_neighborhood_oppo = 0;
    int num_neighborhood_empt = 0;
    int num_neighborhood_boun = 0;
    int Liberties[4];
    int NeighboorhoodState[4];
    count_neighboorhood_state(Board, X, Y, turn,&num_neighborhood_empt,&num_neighborhood_self,&num_neighborhood_oppo,
      &num_neighborhood_boun, NeighboorhoodState);
    if (num_neighborhood_oppo != 0) {
        count_liberty(X, Y, Board, Liberties);
        for (int d = 0 ; d < MAXDIRECTION; ++d) {
            if (NeighboorhoodState[d] == OPPONENT && Liberties[d] == 1 && Board[X+DX[d]][Y+DY[d]]!=EMPTY) {
                remove_piece(Board, X+DX[d], Y+DY[d], Board[X+DX[d]][Y+DY[d]]);
            }
        }
    }
    Board[X][Y] = turn;
}

void init_liberty(int Board[BOUNDARYSIZE][BOUNDARYSIZE],int AllStrings[BOUNDARYSIZE][BOUNDARYSIZE],int StringLiberties[90], int StringNum[90],int StringHash[90],int StringStack[90],int &stk,int &hash,int &total_num){
    stk=0;
    hash=0;
    total_num=0;
    for(int i=0;i<90;i++) {
        StringNum[i] = 0;
        StringHash[i]=0;
        StringLiberties[i]=0;
        StringStack[stk++]=i;
    }
    int in=0;
    pair<int,int>input[200];
    for (int i=1;i<=BOARDSIZE;i++) {
		    for (int j=1;j<=BOARDSIZE;j++) {
            if(Board[i][j]==WHITE||Board[i][j]==BLACK) {
                total_num++;
                input[in++]=make_pair(i,j);
            }
            hash+=HashBoard[Board[i][j]][i][j];
			  }
		}
    find_all_liberty(Board,StringLiberties,AllStrings,StringNum,StringHash,input,in,StringStack,stk);
}
//int gen_legal_move(int Board[BOUNDARYSIZE][BOUNDARYSIZE],int turn,int MoveList[HISTORYLENGTH],bool returnsoon=false){
int gen_legal_move(int Board[BOUNDARYSIZE][BOUNDARYSIZE],int turn,int MoveList[HISTORYLENGTH],
   int AllStrings[BOUNDARYSIZE][BOUNDARYSIZE],int StringLiberties[90], int StringNum[90], int StringHash[90],int hash,int total_num,bool returnsoon = false){
    int num_neighborhood_self = 0;
    int num_neighborhood_oppo = 0;
    int num_neighborhood_empt = 0;
    int num_neighborhood_boun = 0;
    int legal_moves = 0;
    int Liberties[4];
    int NeighboorhoodState[4];
    bool eat_move = 0;
    bool ok = 0; 
    int sp = rand()%81;
    for(int p=0;p<81;p++){
          int x=xyArray[p+sp].first;
          int y=xyArray[p+sp].second;
	          if (Board[x][y] == 0) {
                num_neighborhood_self = 0;
                num_neighborhood_oppo = 0;
                num_neighborhood_empt = 0;
                num_neighborhood_boun = 0;
                count_neighboorhood_state(Board, x, y, turn,&num_neighborhood_empt,&num_neighborhood_self,
                &num_neighborhood_oppo,&num_neighborhood_boun, NeighboorhoodState);
		            ok = 0;
                eat_move = 0;
                for (int d = 0 ; d < MAXDIRECTION; ++d) {
                    if(Board[x+DX[d]][y+DY[d]]==BLACK||Board[x+DX[d]][y+DY[d]]==WHITE)
                        Liberties[d] = StringLiberties[AllStrings[x+DX[d]][y+DY[d]]];
                    else Liberties[d] = 0;
                }
                if (num_neighborhood_empt > 0) {
                    ok = 1;  
                    for (int d = 0 ; d < MAXDIRECTION; ++d) 
                        if (NeighboorhoodState[d] == OPPONENT && Liberties[d] == 1) {
                            eat_move = 1;
                            break;
                        }
		            }
		            else {
		                if (num_neighborhood_self + num_neighborhood_boun == MAXDIRECTION) {
			                  int check_flag = 0, check_eye_flag = num_neighborhood_boun;
			                  for (int d = 0 ; d < MAXDIRECTION; ++d) {
			                      if (NeighboorhoodState[d]==SELF && Liberties[d] > 1) {
				                        check_eye_flag++;
                                check_flag = 1;
			                      }
                        }
                        if (check_flag == 1 && check_eye_flag!=4) ok=1;
                    }       	
                    else if (num_neighborhood_oppo > 0) {
                        bool check_flag = 0;
                        for (int d = 0 ; d < MAXDIRECTION; ++d) {
			                      if (NeighboorhoodState[d]==SELF && Liberties[d] > 1) check_flag = 1;
			                      if (NeighboorhoodState[d]==OPPONENT && Liberties[d] == 1) {
                                eat_move = 1;
                                break;
                            }
			                  }
                        ok = eat_move|check_flag;
		                }           	
		            }
		            if (ok) {
                    int remove_id[4],r=0;
                    int now_hash = hash;
                    int remove_num=0;
                    if (eat_move == 1) {
		                    for(int d=0;d<MAXDIRECTION;d++){
			                      if (NeighboorhoodState[d]==OPPONENT && Liberties[d] == 1) {
                                int id = AllStrings[x+DX[d]][y+DY[d]];
                                remove_id[r++]=id;
                            } 
                        }
                        sort(remove_id,remove_id+r);
                        for(int s=0;s<r;s++){
                            if(s>0&&remove_id[s]==remove_id[s-1]) continue;
                            remove_num += StringNum[remove_id[s]];
                            now_hash -= StringHash[remove_id[s]];
                        }
                    }
                    now_hash += HashBoard[turn][x][y];
                    int now_num = total_num + 1 - remove_num;
		                bool repeat_move = 0;
                    for(int s=0;s<RecordSize[now_num];s++) if(RecordHash[now_num][s] == now_hash) repeat_move = 1;
                    if (repeat_move == 0) {
                        int deadmove = 0;
                        if(num_neighborhood_self==0&&num_neighborhood_empt==1) deadmove=1;
			                  MoveList[legal_moves] =  eat_move * 100 +x * 10 + y ;
			                  legal_moves++;
                        if(returnsoon) return legal_moves;
		                }
		            }
	          }
	      }
    return legal_moves;
}

double final_score(int Board[BOUNDARYSIZE][BOUNDARYSIZE]) {
    int black, white;
    black = white = 0;
    int is_black, is_white;
    for (int i = 1 ; i <= BOARDSIZE; ++i) {
	      for (int j = 1; j <= BOARDSIZE; ++j) {
	          switch(Board[i][j]) {
		            case EMPTY:
		                is_black = is_white = 0;
		                for(int d = 0 ; d < MAXDIRECTION; ++d) {
			                  if (Board[i+DX[d]][j+DY[d]] == BLACK) is_black = 1;
			                  if (Board[i+DX[d]][j+DY[d]] == WHITE) is_white = 1;
		                }
		                if (is_black + is_white == 1) {
			                  black += is_black;
			                  white += is_white;
		                }
		                break;
                case WHITE:
                    white++;
                    break;
                case BLACK:
                    black++;
                    break;
            }
        }
    }
    return black - white;
}

void do_move(int Board[BOUNDARYSIZE][BOUNDARYSIZE], int turn, int move) {
    int move_x = (move % 100) / 10;
    int move_y = move % 10;
    if ((move%1000)<100) {
	      Board[move_x][move_y] = turn;
    }
    else {
	      update_board(Board, move_x, move_y, turn);
    }
}

void myrecord(int Board[BOUNDARYSIZE][BOUNDARYSIZE]) {
		int num=0;
    int h=0;
    for (int i=1 ; i<=BOARDSIZE; ++i) {
		    for (int j=1 ; j<=BOARDSIZE; ++j) {
            if(Board[i][j]==BLACK||Board[i][j]==WHITE) num++;
            h+=HashBoard[Board[i][j]][i][j];
		    }
		}
    numberHash[total_hash_num]=num;
    total_hash_num++;
    RecordHash[num][RecordSize[num]] = h;
    RecordSize[num]++;
    assert(RecordSize[num]<2000);
    assert(total_hash_num<2000);
}
void record(int Board[BOUNDARYSIZE][BOUNDARYSIZE], int GameRecord[MAXGAMELENGTH][BOUNDARYSIZE][BOUNDARYSIZE], int game_length) {
    int num=0;
    for (int i = 0 ; i < BOUNDARYSIZE; ++i) {
		    for (int j = 0 ; j < BOUNDARYSIZE; ++j) {
			      GameRecord[game_length][i][j] = Board[i][j];
		    }
		}
    myrecord(Board);
}

void my_remove_piece(int Board[BOUNDARYSIZE][BOUNDARYSIZE], int X, int Y, int turn,pair<int,int>input[200],int &in,int &hash,int &total_num,int ids[100],int &idn,int AllStrings[BOUNDARYSIZE][BOUNDARYSIZE]) {
    if(Board[X][Y]!=EMPTY){
        total_num-=1;
        hash-=HashBoard[turn][X][Y];
    }
    Board[X][Y] = EMPTY;
    for (int d = 0; d < MAXDIRECTION; ++d) {
	      if (Board[X+DX[d]][Y+DY[d]] == turn) {
	          my_remove_piece(Board,X+DX[d],Y+DY[d],turn,input,in,hash,total_num,ids,idn,AllStrings);
	      }
        else if (Board[X+DX[d]][Y+DY[d]] == 3-turn){ 
            int id=AllStrings[X+DX[d]][Y+DY[d]];
            if(!StringFlag[id]){
                input[in++]=make_pair(X+DX[d],Y+DY[d]);
                StringFlag[id]=true;
                ids[idn++]=id;
            }
        }
    }
}

void update_move(int Board[BOUNDARYSIZE][BOUNDARYSIZE], int turn, int move,int AllStrings[BOUNDARYSIZE][BOUNDARYSIZE],
    int StringLiberties[90], int StringNum[90],int StringHash[90],int &total_num,int& hash
    ,int StringStack[90],int &stk){
    int move_x = (move % 100) / 10;
    int move_y = move % 10;
    int ids[100];
    for(int i=0;i<90;i++) StringFlag[i]=false;
    pair<int,int>input[200];
    int in=0;
    input[in++]=make_pair(move_x,move_y);
    int idn=0;
    for(int d=0;d<4;d++){
        int dx=DX[d];
        int dy=DY[d];
        if(Board[move_x+dx][move_y+dy]==turn){
            int id=AllStrings[move_x+dx][move_y+dy];
            if(StringFlag[id]==false){
                StringFlag[id]=true;
                ids[idn++]=id;
                input[in++]=make_pair(move_x+dx,move_y+dy);
            }
        }
    }
    if((move%1000) >= 100){
        for(int d=0;d<4;d++){
            int dx=DX[d];
            int dy=DY[d];
            if(Board[move_x+dx][move_y+dy] == 3-turn && StringLiberties[AllStrings[move_x+dx][move_y+dy]]==1) 
              my_remove_piece(Board,move_x+dx,move_y+dy,3-turn,input,in,hash,total_num,ids,idn,AllStrings);
        }
    }
    Board[move_x][move_y] = turn;
    hash+=HashBoard[turn][move_x][move_y];
    total_num++;
    for(int i=0;i<idn;i++){
        StringFlag[ids[i]]=false;
        StringStack[stk++]=ids[i];
    }
    idn=0;
    for(int d=0;d<4;d++){
        int dx=DX[d];
        int dy=DY[d];
        if(Board[move_x+dx][move_y+dy]==3-turn){
            int id=AllStrings[move_x+dx][move_y+dy];
            if(StringFlag[id]==false){
                StringFlag[id]=true;
                ids[idn++]=id;
                StringLiberties[id]--;
            }
        }
    }
    for(int i=0;i<idn;i++)  StringFlag[ids[i]]=false;
    for(int i=0;i<90;i++) assert(StringFlag[i]==false);
    find_all_liberty(Board,StringLiberties,AllStrings,StringNum,StringHash,input,in,StringStack,stk);
}

double simulate(int Board[BOUNDARYSIZE][BOUNDARYSIZE],int turn,int & NumTemp){
    cnt++;
    int MoveList[HISTORYLENGTH];
    int num_legal_moves = 0;
    int ret_move = 0;
    int total_num=0;
    int hash = 0;
    int AllStrings[BOUNDARYSIZE][BOUNDARYSIZE];
    int StringLiberties[90];
    int StringNum[90];
    int StringHash[90];
    int Stack[90];
    int stk = 0;
    init_liberty(Board,AllStrings,StringLiberties,StringNum,StringHash,Stack,stk,hash,total_num);
    while(true){
        turn = 3-turn;
        num_legal_moves = //gen_legal_move(Board, turn, MoveList, true);
          gen_legal_move(Board, turn, MoveList,AllStrings,StringLiberties,StringNum,StringHash,hash,total_num,true);
        if(num_legal_moves == 0) break;
        ret_move = MoveList[0];
        update_move(Board,turn,ret_move,AllStrings,StringLiberties,StringNum,StringHash,total_num,hash,Stack,stk);
        myrecord(Board);
        NumTemp++;
    }
    double calc = final_score(Board)-_komi;
    return calc;
}

int pick_move(int Board[BOUNDARYSIZE][BOUNDARYSIZE],int turn,double timelimit) {
    clock_t start_t,now_t;
    start_t = clock();
    int NextBoard[BOUNDARYSIZE][BOUNDARYSIZE];
    int NewBoard[BOUNDARYSIZE][BOUNDARYSIZE];
    
    int outNumTemp = 0;
    int true_turn = turn;
     
    int runtime = 10;
    int runlimit = 1000;
    //runtime = 0;
    root = new node();
    root->isleaf = true;

    while(1){
        runtime++;
        now_t = clock();
        if( (now_t-start_t) >= CLOCKS_PER_SEC*timelimit || runtime==1000) break;
        outNumTemp = 0;
        turn = true_turn;
        node *now = root;
        for(int i=0 ;i<BOUNDARYSIZE; ++i)
            for (int j=0;j<BOUNDARYSIZE; ++j)
                NextBoard[i][j] = Board[i][j];
        while(now->isleaf == false){
            int id=0;
            double mx=-100;
            for(int i=0;i<now->num_legal_moves;i++){
                if(now->enable[i]==false) continue;
                double calc = (double)(now->child[i]->win_play) / (double)(now->child[i]->total_play);
                if(turn == 3-true_turn) calc = 1.0-calc;
                calc += sqrt(CONSTANT*log((double)(now->total_play))/(double)(now->child[i]->total_play));
                if(calc > mx){
                    id = i;
                    mx = calc;
                }
            }
            do_move(NextBoard,turn,now->MoveList[id]);
            now = now->child[id];
            myrecord(NextBoard);
            outNumTemp++;
            turn = 3-turn;
        }
        int StringLiberties[90];
        int StringNum[90];
        int StringHash[90];
        int StringStack[90];
        int stk = 0;
        int total=0;
        int hash = 0;
        int AllStrings[BOUNDARYSIZE][BOUNDARYSIZE];

        init_liberty(NextBoard,AllStrings,StringLiberties,StringNum,StringHash,StringStack,stk,hash,total);
        now->num_legal_moves = //gen_legal_move(NextBoard, turn, now->MoveList);
          gen_legal_move(NextBoard, turn, now->MoveList,AllStrings,StringLiberties,StringNum,StringHash,hash,total);
        now->isleaf = false;
        if(now->num_legal_moves != 0){
            int total_play = 0;
            int total_win_play = 0;
            int T=1000;
            double max_lower_bound = -1e9;
            for(int i=0;i<now->num_legal_moves;i++) {
                now->child[i] = new node();
                now->child[i]->father = now;
                now->child[i]->isleaf = true;
                int t=T;
                while(t--){
                    for (int _i=0;_i<BOUNDARYSIZE;++_i)
                        for (int _j=0;_j<BOUNDARYSIZE;++_j)
                             NewBoard[_i][_j] = NextBoard[_i][_j];
                    do_move(NewBoard,turn,now->MoveList[i]);
                    myrecord(NewBoard);
                    int inNumTemp = 1;
                    double result = simulate(NewBoard,turn,inNumTemp);
                    now->child[i]->piece_sum+=result;
                    now->child[i]->piece_square+=(result*result);
                    for(int j=0;j<inNumTemp;j++) {
                        int num = numberHash[total_hash_num-1];
                        total_hash_num--;
                        RecordSize[num]--;
                    }
                    if((result>=0)^(true_turn == WHITE)) now->child[i]->win_play++;
                }
                now->child[i]->total_play+=T;
                total_win_play+=now->child[i]->win_play;
                double mean = now->child[i]->piece_sum / (double)(now->child[i]->total_play);
                double sig = sqrt(now->child[i]->piece_square / (double)(now->child[i]->total_play) - mean*mean);
                now->child[i]->lower = mean-rd*sig;
                now->child[i]->upper = mean+rd*sig;
                if(now->child[i]->lower > max_lower_bound) max_lower_bound = now->child[i]->lower;
            }
            total_play+=(T*now->num_legal_moves);
            for(int i=0;i<now->num_legal_moves;i++){
                if(max_lower_bound > now->child[i]->upper) {
                    now->enable[i] = false;
                    calctime++;
                }
            }
            while(true){
                now->win_play += total_win_play;
                now->total_play += total_play;
                if(now == root) break;
                now = now->father;
            }
            for(int i=0;i<outNumTemp;i++) {
                int num = numberHash[total_hash_num-1];
                total_hash_num--;
                RecordSize[num]--;
            }
        }
        else {
            for(int i=0;i<outNumTemp;i++) {
                int num = numberHash[total_hash_num-1];
                total_hash_num--;
                RecordSize[num]--;
            }
            break;
        }
    }
    if(root->num_legal_moves == 0) {
        deleteNode(root);
        return 0;
    }
    double out_mx=-1;
    int ans_id=0;
    bool has_eat=false;
    for(int i=0;i<root->num_legal_moves;i++){
        if((root->MoveList[i]%1000)>=100){
            has_eat = true;
            break;
        }
    }
    for(int i=0;i<root->num_legal_moves;i++){
        if(root->enable[i]==false) continue;
        if(has_eat && (root->MoveList[i]%1000)<100) continue;
        double calc = (double)(root->child[i]->win_play) / (double)(root->child[i]->total_play);
        if(root->MoveList[i]>=1000) calc/=2;
        if(calc > out_mx){
            out_mx = calc;
            ans_id = i;
        }
    }
    if(out_mx == -1){
        deleteNode(root);
        return 0;
    }
    int ans = root->MoveList[ans_id];
    deleteNode(root);
    cerr << runtime << endl;;
    return ans;
}

int genmove(int Board[BOUNDARYSIZE][BOUNDARYSIZE], int turn, int time_limit) {
    int return_move = pick_move(Board, turn,(double)(time_limit)*0.9);
    do_move(Board, turn, return_move);
    return return_move % 100;
}

const char *KnownCommands[]={
    "protocol_version",
    "name",
    "version",
    "known_command",
    "list_commands",
    "quit",
    "boardsize",
    "clear_board",
    "komi",
    "play",
    "genmove",
    "undo",
    "quit",
    "showboard",
    "final_score"
};

void gtp_final_score(int Board[BOUNDARYSIZE][BOUNDARYSIZE]) {
    double result;
    result = final_score(Board);
    result -= _komi;
    cout << "= ";
    if (result > 0.0) { // Black win
	      cout << "B+" << result << endl << endl<< endl;;
    }
    else if (result < 0.0) { // White win
	      cout << "W+" << -result << endl << endl<< endl;;
    }
    else { // draw
	      cout << "0" << endl << endl<< endl;;
    }
}
void gtp_undo(int Board[BOUNDARYSIZE][BOUNDARYSIZE], int game_length, int GameRecord[MAXGAMELENGTH][BOUNDARYSIZE][BOUNDARYSIZE]) {
    if (game_length!=0) {
	      for (int i = 1; i <= BOARDSIZE; ++i) {
	          for (int j = 1; j <= BOARDSIZE; ++j) {
		            Board[i][j] = GameRecord[game_length][i][j];
	          }
	      }
        if(total_hash_num >= 1){
            int num = numberHash[total_hash_num-1];
            total_hash_num--;
            RecordSize[num]--;
        }
    }
    cout << "= " << endl << endl;
}
void gtp_showboard(int Board[BOUNDARYSIZE][BOUNDARYSIZE]) {
    for (int i = 1; i <=BOARDSIZE; ++i) {
	      cout << "#";
	      cout <<10-i;
	      for (int j = 1; j <=BOARDSIZE; ++j) {
	          switch(Board[i][j]) {
		            case EMPTY: cout << " .";break;
		            case BLACK: cout << " X";break;
		            case WHITE: cout << " O";break;
	          }
	      }
	      cout << endl;
    }
    cout << "#  ";
    for (int i = 1; i <=BOARDSIZE; ++i)  cout << LabelX[i] <<" ";
    cout << endl;
    cout << endl;
}
void gtp_protocol_version() {
    cout <<"= 2"<<endl<< endl;
}
void gtp_name() {
    cout <<"= Go" << endl<< endl;
}
void gtp_version() {
    cout << "= 1.02" << endl << endl;
}
void gtp_list_commands(){
    cout <<"= ";
    for (int i = 0 ; i < NUMGTPCOMMANDS; ++i) {
	      cout <<KnownCommands[i] << endl;
    }
    cout << endl;
}
void gtp_known_command(const char Input[]) {
    for (int i = 0 ; i < NUMGTPCOMMANDS; ++i) {
	      if (strcmp(Input, KnownCommands[i])==0) {
	          cout << "= true" << endl<< endl;
	          return;
	      }
    }
    cout << "= false" << endl<< endl;
}
void gtp_boardsize(int size) {
    if (size!=9) {
	      cout << "? unacceptable size" << endl<< endl;
    }
    else {
	      _board_size = size;
	      cout << "= "<<endl<<endl;
    }
}
void gtp_clear_board(int Board[BOUNDARYSIZE][BOUNDARYSIZE], int NumCapture[]) {
    reset(Board);
    NumCapture[BLACK] = NumCapture[WHITE] = 0;
    cout << "= "<<endl<<endl;
}
void gtp_komi(double komi) {
    _komi = komi;
    cout << "= "<<endl<<endl;
}
void gtp_play(char Color[], char Move[], int Board[BOUNDARYSIZE][BOUNDARYSIZE], int game_length, int GameRecord[MAXGAMELENGTH][BOUNDARYSIZE][BOUNDARYSIZE]) {
    int turn, move_i, move_j;
    if (Color[0] =='b' || Color[0] == 'B')
	      turn = BLACK;
    else
	      turn = WHITE;
    if (strcmp(Move, "PASS") == 0 || strcmp(Move, "pass")==0) {
	      record(Board, GameRecord, game_length+1);
    }
    else {
	      // [ABCDEFGHJ][1-9], there is no I in the index.
	      Move[0] = toupper(Move[0]);
	      move_j = Move[0]-'A'+1;
	      if (move_j == 10) move_j = 9;
	      move_i = 10-(Move[1]-'0');
	      update_board(Board, move_i, move_j, turn);
	      record(Board, GameRecord, game_length+1);
    }
    cout << "= "<<endl<<endl;
}

void gtp_genmove(int Board[BOUNDARYSIZE][BOUNDARYSIZE], char Color[], int time_limit, int game_length, int GameRecord[MAXGAMELENGTH][BOUNDARYSIZE][BOUNDARYSIZE]){
    int turn = (Color[0]=='b'||Color[0]=='B')?BLACK:WHITE;
    int move = genmove(Board, turn, time_limit);
    int move_i, move_j;
    record(Board, GameRecord, game_length+1);
    if (move==0) {
	      cout << "= PASS" << endl<< endl<< endl;
    }
    else {
	      move_i = (move%100)/10;
	      move_j = (move%10);
	      cout << "= " << LabelX[move_j]<<10-move_i<<endl<< endl;
    }
}

void gtp_main(int display) {
    char Input[COMMANDLENGTH]="";
    char Command[COMMANDLENGTH]="";
    char Parameter[COMMANDLENGTH]="";
    char Move[4]="";
    char Color[6]="";
    int ivalue;
    double dvalue;
    int Board[BOUNDARYSIZE][BOUNDARYSIZE]={{0}};
    int NumCapture[3]={0};// 1:Black, 2: White
    int time_limit = DEFAULTTIME;
    int GameRecord[MAXGAMELENGTH][BOUNDARYSIZE][BOUNDARYSIZE]={{{0}}};
    int game_length = 0;
    if (display==1) {
        gtp_list_commands();
        gtp_showboard(Board);
    }
    while (fgets(Input,COMMANDLENGTH,stdin) != 0) {
	      sscanf(Input, "%s", Command);
  	    if (Command[0]== '#') continue;
        if (strcmp(Command, "protocol_version")==0) {
            gtp_protocol_version();
        }
        else if (strcmp(Command, "name")==0) {
            gtp_name();
        }
        else if (strcmp(Command, "version")==0) {
            gtp_version();
        }
        else if (strcmp(Command, "list_commands")==0) {
            gtp_list_commands();
        }
        else if (strcmp(Command, "known_command")==0) {
            sscanf(Input, "known_command %s", Parameter);
            gtp_known_command(Parameter);
        }
        else if (strcmp(Command, "boardsize")==0) {
            sscanf(Input, "boardsize %d", &ivalue);
            gtp_boardsize(ivalue);
        }
        else if (strcmp(Command, "clear_board")==0) {
            gtp_clear_board(Board, NumCapture);
            game_length = 0;
            total_hash_num = 0;
            for(int i=0;i<100;i++) RecordSize[i]=0;
        }
        else if (strcmp(Command, "komi")==0) {
            sscanf(Input, "komi %lf", &dvalue);
            gtp_komi(dvalue);
        }
        else if (strcmp(Command, "play")==0) {
            sscanf(Input, "play %s %s", Color, Move);
            gtp_play(Color, Move, Board, game_length, GameRecord);
            game_length++;
            if (display==1) {
                gtp_showboard(Board);
            }
        }
        else if (strcmp(Command, "genmove")==0) {
            sscanf(Input, "genmove %s", Color);
            gtp_genmove(Board, Color, time_limit, game_length, GameRecord);
            game_length++;
            if (display==1) {
                gtp_showboard(Board);
            }
        }
        else if (strcmp(Command, "quit")==0) {
            break;
        }
        else if (strcmp(Command, "showboard")==0) {
            gtp_showboard(Board);
        }
        else if (strcmp(Command, "undo")==0) {
            game_length--;
            gtp_undo(Board, game_length, GameRecord);
            if (display==1) {
                gtp_showboard(Board);
            }
        }
        else if (strcmp(Command, "final_score")==0) {
            if (display==1) {
                gtp_showboard(Board);
            }
            gtp_final_score(Board);
        }
    }
}
int main(int argc, char* argv[]) {
    //srand(time(NULL)^getpid());
    srand(217217);
    alltime = 0;
    calctime = 0;
    int b=BLACK,w=WHITE;
    int tmpcnt = 0;
    F=E=0;
    for(int i=BOARDSIZE;i>=1;i--)
        for(int j=BOARDSIZE;j>=1;j--){
            xyArray[tmpcnt++] = make_pair(i,j);
            HashBoard[BLACK][i][j] = b;
            HashBoard[WHITE][i][j] = w;
            HashBoard[EMPTY][i][j] = 0;
            b*=3;
            w*=3;
            flag[i][j]=0;
            empty[i][j]=0;
        }
    for(int i=0;i<tmpcnt;i++) xyArray[tmpcnt+i]=xyArray[i];
    total_hash_num = 0;
    for(int i=0;i<90;i++) {
        RecordSize[i]=0;
        StringFlag[i]=false;
    }
    int type = GTPVERSION;// 1: local version, 2: gtp version
    int display = 0; // 1: display, 2 nodisplay
    if (argc > 1) {
        if (strcmp(argv[1], "-display")==0) display = 1;
        if (strcmp(argv[1], "-nodisplay")==0) display = 0;
    }
    gtp_main(display);
    //cerr << calctime <<endl;
    //cerr << alltime <<endl;
    cerr << cnt <<endl;
}
